<?php
//pure PHP file nincs záró eleme a PHP tag-nek
/*
 * A PHP rövidítése rekurzív :)
PHP
Hypertext
Pre-processor
*/
//változók a PHP ban - primitívek
$egesz = 5;// operátor = -> értékadás | integer (int)
$tort = 6/7;// floating point (float)
$logikai = true;//boolean (bool) true - false
$szoveg = "Ez egy szöveg";//string

//változó érték kiírása (primitivek)
echo $egesz;
//hogy a dumpok formázottan látszódjanak:
echo '<pre>';
//változók értékének és tipusának kiírása (csak fejlesztés alatt!!!)
var_dump($egesz,$tort,$logikai,$szoveg);

//nem primitiv változók
$tomb = [];//üres tömb megadása
$tomb = [ 345, 5/9, false, 'A nevem: Horváth György' ];//tömb megadása alap értékekkel

var_dump($tomb);

//tömb 1 eleme adott indexről
var_dump($tomb[3]);

//objektum a PHP ban
$objektum = (object) $tomb;//tipuskényszerítés - typecasting
var_dump($objektum);//PHP ban objektum csak osztályból keletkezhet

//Állandók a PHP ban
const ALLANDO_NEVE = 21;
var_dump(ALLANDO_NEVE);
//ALLANDO_NEVE = 22;//állandót nem módosíthatsz!!!