<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP alapok - az IF-ELSE Elágazás és FOR ciklus </title>
</head>
<body>
<h1>Az elágazás</h1>
<?php
//generáljunk egy számot 1 éa 100 között egy változóba és írjuk ki hogy áros vagy páratlan -e
$veletlenSzam = rand(1,100);//camelCase változónév snake_case változónév
echo 'A generált szám: ' . $veletlenSzam;//operátor . -> konkatenáció , a két oldalán található kifejezést string-ként összefűzi

//elágazás annak vizsgálatára hogy a generált szám páros -e maradékos osztással
/*
if(condition){
    //true
}else{
    //false
}
 */
if( $veletlenSzam%2 == 0 ){
    //páros
    echo "<br>ami páros.";
}else{
    //páratlan
    echo "<br>ami páratlan.";
}


//A for ciklus a PHP ban
/*
for(ciklusváltozó kezdeti értéke;ciklusváltozó vizsgálata (belépési feltétel);ciklusváltozó léptetése){
    //ciklusmag

}
*/
//írjuk ki 1-10ig a számokat
for( $i=1 ; $i<=10 ; $i++){
    echo '<br>'.$i;
    //mégegyszer kiírjuk
    echo "<br>$i";//"" - en belül a primitivek az értékükkel vannak reprezentálva
    //harmadszor is kiírjuk :)
    echo "<br>\"\$i\"";//operátor \ -> escape operátor, az utána következő elemet kiveszi a nyelvi végrehajtásból
}

echo "<script>console.log('helo')</script>";

?>
</body>
</html>
