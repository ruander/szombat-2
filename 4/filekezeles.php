<?php

//ha létezik a file, olvassuk be (adatok/user.txt) is_file(), file_get_contents(), egyébként írjuk ki hogy nem létezik a file: (adatok/user.txt)
$dir = 'adatok/';
$filename = 'users.json';
if(is_file($dir.$filename)){//ha létezik a file
    $fileContents = file_get_contents($dir.$filename);//filetartalom beolvasás
    $data = json_decode($fileContents, true);//visszaalakítjuk tömbbé

    //unlink($dir.$filename);//file törlése
    //rmdir($dir);//mappa törlése - csak üres mappa törölhető!!!

    //var_dump('<pre>',$data);//array(3) ....
    $table = '<div><a href="../3/urlap.php">Új felvitel</a></div>';//új felvitel
    $table .= '<table border="1">';//tablázat nyitás
    $table .= '<tr>
                <th>id</th>
                <th>név</th>
                <th>email</th>
                <th>jelszó hash</th>
                <th>művelet</th>
              </tr>';//táblázat fejléc

    //ciklussal bejárjuk az adattömböt és beillesztjük a cellákba az adatokat
    foreach($data as $id => $user){
        $table .= '<tr>
                <td>'.$id.'</td>
                <td>'.$user['name'].'</td>
                <td>'.$user['email'].'</td>
                <td>'.$user['password'].'</td>
                <td>
                  <a href="?action=update&amp;id='.$id.'">módosít</a>
                  <a href="?action=delete&amp;id='.$id.'" onclick="return confirm(\'Tuti?\')">töröl</a>
                </td>
              </tr>';//táblázat fejléc
    }

    $table .= '</table>';//táblázat zárás
    echo $table;//kiírás egy lépésben
}else{
    //átirányítás
    header('location:../3/urlap.php');//header előtt nem írhatsz ki semmit!!!
    exit();//die();
    echo 'Nincs ilyen file: '.$dir.$filename;
    //echo '<div><a href="../3/urlap.php">űrlap</a></div>';ha nincs ilyen file kattinthasson az űrlapra
}
