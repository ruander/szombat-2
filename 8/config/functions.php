<?php
//Saját eljárások gyüjteménye
/**
 * @param $str
 * @return string
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * Beléptető eljárás
 * @return bool
 */
function login()
{
    global $link;//link globálissá állítása az eljárás idejére

    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');
//lekérjük a jelszót
    $qry = "SELECT password, id, name FROM users WHERE email = '$email' AND status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);//ha jó az email, kapunk egy jelszót hozzá a row[0]ba vagy a row NULL
//var_dump($row);
    if (
        $row !== null //kell hogy legyen mert fogjuk használni a [0] indexet
        and
        password_verify($password, $row['password'])
    ) {
        //mikor történik a beléptetés
        $stime = time();//mp alapú UNIX timestamp
        //milyen mf azonosítóval, jelszó?
        $sid = session_id();
        $spass = md5($sid . $row['id'] . SECRET_KEY);//md5(mf azonosito+userID+secret)
        // és ki lépett be (lekértük a row-ba)
        unset($row['password']);//jelszó a továbbiakban nem kell
        $_SESSION['userdata'] = $row;
        $_SESSION['userdata']['email'] = $email;
        //'beragadt' belépés tisztítása a mf-ra
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        //mf tárolása adatbázisba
        $qry = "INSERT INTO sessions(sid,spass,stime) VALUES('$sid','$spass',$stime)";
        mysqli_query($link, $qry) or die(mysqli_error($link));

        return true;

    }

    return false;
}

/**
 * Érvényes belépés utáni azonosítás
 * @return bool
 */
function auth(){
    global $link;
    $spass = null;
    $sid = session_id();
    //vannak-e felh adatok?
    if(isset($_SESSION['userdata'])){
        $spass = md5($sid.$_SESSION['userdata']['id'].SECRET_KEY);
    }
    $now = time();//ennyi most az idő :)
    //lejárt munkafolyamatok max timestampje
    $expired = $now-(15*60);
    //öntisztító tábla
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $expired ") or die(mysqli_error($link));//lejárt bejelentkezések törlése
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    if( isset($row) && $spass === $row[0]){
        //echo 'Sikeres azonosítás';
        //stime update
        mysqli_query($link,"UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }

    return false;
}

/**
 * Kiléptetés
 * @return void
 */
function logout(){
    global $link;
    //roncsoljuk a mf elmeit
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' ") or die(mysqli_error($link));//db bejegyzés törlése
    $_SESSION = [];//tömb ürítése
    session_destroy();//mf roncsolás
}

/**
 * Input mezők value értékeit adja vissza ha ki volt töltve
 * @param $fieldName | mező name tulajdonsága ... name="fieldname"
 * @param array $dataRow | az adatbázis lekérés kibontot asszociativ tömbje (mezőnév = db fieldname !)
 * @return mixed
 */
function getValue($fieldName, $dataRow = [])
{
    $ret = false;
    if(filter_input(INPUT_POST, $fieldName)){//ha postban van adat az az elsődleges
        $ret = filter_input(INPUT_POST, $fieldName);
    }elseif(array_key_exists($fieldName, $dataRow)){//adatbázisból ha van adat...
        $ret = $dataRow[$fieldName];
    }

    return $ret;
}

/**
 * Hibakiíró eljárás
 * $hiba['fieldname'] = 'Hibaüzenet' szerkezetből
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a $hiba tömb globális lesz
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}
