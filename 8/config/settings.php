<?php
//beállítások
const SECRET_KEY = 'S3cR3T_K3y!';//rendszer tikosító kulcs
const MODULE_DIR = 'modules/';//itt vannak a modulok
const MODULE_EXT = '.php';//most ilyen kiterjesztéssel

const ADMIN_MENU = [
    0 => [
        'title' => 'Vezérlőpult',
        'module' => 'dashboard',
        'icon' => 'fa fa-dashboard'
    ],
    1 => [
        'title' => 'Cikkek',
        'module' => 'articles',
        'icon' => 'fa fa-valami-fancy-ikon'
    ],

    10 => [
        'title' => 'Felhasználók',
        'module' => 'users',
        'icon' => 'fa fa-user'
    ],
    //.....
];
