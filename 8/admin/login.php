<?php
//belépés űrlap
require "../config/connect.php";//adatbázis kapcsolat betöltése
require "../config/functions.php";//saját eljárások betöltése
require "../config/settings.php";//beállítások betöltése
//munkafolyamat
session_start();//mf indítása

$info = 'Írja be a belépési adatokat!';

if (!empty($_POST)) {
    if (login()) {
        header('location:index.php');//beléptetés kész mehetünk az indexre...
        exit();
    } else {
        $info = 'Érvénytelen email/jelszó páros!';
    }
}



?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztráció belépés</title>
</head>
<body>
<form method="post">
    <h1>Adminisztráció - Belépés</h1>
    <div class="message">
        <?php echo $info; ?>
    </div>
    <label>
        <span>Email</span>
        <input type="text" name="email" value="<?php echo getValue('email'); ?>" placeholder="email@cim.hu">
    </label>
    <label>
        <span>Jelszó</span>
        <input type="password" name="password" value="" placeholder="******">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>
<?php

//echo '<pre>'.var_export($_SERVER,true).'</pre>';
