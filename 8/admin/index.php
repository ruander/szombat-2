<?php
require "../config/connect.php";//adatbázis kapcsolat betöltése
require "../config/functions.php";//saját eljárások betöltése
require "../config/settings.php";//beállítások betöltése
//munkafolyamat
session_start();//mf indítása

//kiléptetünk ha kell
if(filter_input(INPUT_GET, 'logout') !== NULL ){
    logout();
}
$auth = auth();//azonosítás
//die();
if(!$auth){
    header('location:login.php');
    exit();
}
//urlből kialakítjuk milyen modul kell
$o = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//ha nincs, legyen 0
//csak az ADMIN_MENU indexek érkezhetnek
if(!array_key_exists($o,ADMIN_MENU)){
    $o=0;
}
//scriptek itt gyűlnek
$additional_scripts = '';
$additional_styles = '';
$baseURL = 'index.php?p='.$o;//slsp url a modul azonosítóval
//modul betöltése ($output kialakítása)
$moduleFile = MODULE_DIR.ADMIN_MENU[$o]['module'].MODULE_EXT;//modul elérés összeállítása
if(file_exists($moduleFile)){
    include $moduleFile;//output
}else{
    $output = 'Nincs ilyen modul:'.$moduleFile;
}


$userbar = "<div class=\"userbar\">Üdvözöllek <b>{$_SESSION['userdata']['name']}</b> ! | <a href=\"?logout\">kilépés</a></div>";

//menü felépítése (menu render)
$adminmenu =  '<nav><ul>';//menü nyitás
foreach(ADMIN_MENU as $menuId => $menuItem){
    $adminmenu .= '<li class="'.$menuItem['icon'].'"><a href="?p='.$menuId.'">'.$menuItem['title'].'</a></li>';//menü elemek
}
$adminmenu .= '</ul></nav>';//menü zárása

echo $userbar;//felhasználó blokk

echo $adminmenu;//menü hozzáfűzése az outputhoz

echo $output;//module output

