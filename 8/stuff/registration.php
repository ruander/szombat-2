<?php
require "../config/connect.php";//adatbázis kapcsolat betöltése
require "../config/functions.php";//saját eljárások betöltése
/**
 * Regisztrációs űrlap minta
 */
if (!empty($_POST)) {
    //var_dump('<pre>',$_POST);
    //hibakezelés
    $hiba = [];//üres hibatömb, ide gyűjtjük a hibákat
    //...hibák kezelése, tárolása
    //Név min 3 karakter
    $name = trim(filter_input(INPUT_POST, 'name'));
    //$name = trim($name);//spacek eltávolítása mindkét oldalról
    $name = strip_tags($name);//html tagek eltávolítása a szövegből... (védelem)
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //echo $name;

    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    } else {
        //email foglaltság ellenőrzése
        $qry = "SELECT id FROM users WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if ($row) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }
    //jelszó 1
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {//min 6 karakter
        $hiba['password'] = '<span class="error">Hibás formátum - minimum 6 karakter!</span>';
    } elseif ($password !== $repassword) {//jelszó2
        $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';

    } else {
        //jelszó elkódolása
        $password = password_hash($password, PASSWORD_BCRYPT);
    }

    //Státusz, most fix 1
    $status = 0;

    //adatvédelmi elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {//ha nem érkezik az adott eleme a postnak (checkbox!!!)
        $hiba['terms'] = '<span class="error">Kötelező mező!</span>';
    }

    if (empty($hiba)) {
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];
        $data['time_created'] = date('Y-m-d H:i:s');
        //regisztrációnál a státusz fix 1
        //echo '<pre>' . var_export($data, true) . '</pre>';
        //beírás adatbázisba
        $qry = "INSERT INTO 
                        `users` ( 
                                 `name`, 
                                 `email`, 
                                 `password`, 
                                 `status`, 
                                 `time_created`) 
                        VALUES ( 
                                '{$data['name']}', 
                                '{$data['email']}', 
                                '{$data['password']}', 
                                '{$data['status']}', 
                                '{$data['time_created']}');";
        mysqli_query($link, $qry) or die(mysqli_error($link));//kérés futtatása
        //meghivjuk a regsztrációt hogy üres formot kapjunk
        header('location:' . $_SERVER['PHP_SELF']);
        exit();
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap - regisztráció</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 500px;
            margin: auto;
            padding: 15px;
            display: flex;
            flex-flow: column;
        }

        label {
            width: 100%;
            margin: 15px auto 5px;
            display: flex;
            flex-flow: column;
        }

        .error {
            color: #f00;
            font-size: 0.7em;
            font-style: italic;
        }
    </style>
</head>
<body>
<?php
//ürlap elemek gyüjtése stringbe
$form = '<form method="post">
    <h2>Regisztrációs adatlap</h2>';
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="' . getValue('name') . '">' . getError('name') . '</label>';

//email
$form .= '<label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="' . getValue('email') . '">' . getError('email') . '</label>';

//Jelszó
$form .= '<label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">' . getError('password') . '</label>';

//Jelszó újra
$form .= '<label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">' . getError('repassword') . '</label>';

//adatvédelmi tájékoztató
$checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';//volt-e kipipálva?
$form .= '<label><span>';
$form .= '<input type="checkbox" name="terms" value="1" '.$checked.'>
         Elolvastam és megértettem az <a href="#" target="_blank">adatvédelmi tájékoztató</a>ban leírtakat!</span>' . getError('terms') . '</label>';
$form .= '<button>Regisztrálok</button>
        </form>';
echo $form;
?>
</body>
</html>
