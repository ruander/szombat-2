<?php
//(feladatgyujtemeny.pdf) 2. Készítsünk programot, amely kiszámolja az első 100 darab. pozitív egész számok összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)

$sum = 0;//itt növelünk majd
for($i=1;$i<=100;$i++) {
    $sum += $i; //$sum = $sum +$i;//megnöveljük a bal oldali változó értékét a jobb oldali értékével
}

echo "<div>A számok összege 1-100-ig: $sum</div>";
//12.Írjon php ciklust, amely előállít egy X*Y cellát tartalmazó táblázatot és mindegyik cellában a Ruander szót helyezi el.
$x = rand(3,8);//ennyi sort szeretnénk
$y = rand(5,8);//ennyi cellát egy sorba

//a táblázatot összegyűjtjük string-ként egy ávltozóba és amikor kész majd egy lépésben kiírjuk
$table = '<table border="1">';//table tag nyitása
//megoldás egymásba ágyazott ciklusokkal
for($i=1;$i<=$x;$i++) {
    $table .= '<tr>';//sor nyitása

    for ($j = 1; $j <= $y; $j++) {// ciklus a cella és tartalom számára
        $table .= '<td>Ruander</td>';
    }//cella ciklus vége

    $table .= '</tr>';//sor zárása
}//sor ciklus zárása
$table .= '</table>';//table zárás hozzáfűzése az addigi elemekhez

echo $table;//kiírás egy lépésben

//21b.Írjon egy php ciklust, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja a tömb legkisebb és legnagyobb elemét.
$tomb = [];//itt lesznek az elemek
for($i=1;$i<=10;$i++){
    $tomb[] = rand(1,1000);
}
/*
$tomb2 = [];
while(count($tomb2)<10){
    $tomb2[] = rand(1,1000);
}
*/
echo '<pre>'.var_export($tomb,true).'</pre>';
$min = min($tomb);
$max = max($tomb);
echo "<div>A tömb értékeinek minimuma: $min | maximuma: $max</div>";