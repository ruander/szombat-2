<?php
/**
 * Regisztrációs űrlap minta
 */
//ha van valami a postban akkor kell feldolgozni
if (!empty($_POST)) {
    //var_dump('<pre>',$_POST);
    //hibakezelés
    $hiba = [];//üres hibatömb, ide gyűjtjük a hibákat
    //...hibák kezelése, tárolása
    //Név min 3 karakter
    $name = trim(filter_input(INPUT_POST, 'name'));
    //$name = trim($name);//spacek eltávolítása mindkét oldalról
    $name = strip_tags($name);//html tagek eltávolítása a szövegből... (védelem)
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //echo $name;

    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //jelszó 1
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {//min 6 karakter

        $hiba['password'] = '<span class="error">Hibás formátum - minimum 6 karakter!</span>';

    } elseif ($password !== $repassword) {//jelszó2

        $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';

    } else {

        //jelszó elkódolása
        //$password = md5($password.'_s3cr3tp4ssw0rd');//jelszó encrypt <-> decrypt 123456_s3cr3tp4ssw0rd //ne használj jelszó kódolásra md5öt
        $password = password_hash($password, PASSWORD_BCRYPT);//encrypt
        //ellenőrzés: var_dump(password_verify(kapott jelszó, password_hash eredménye));

    }


    //adatvédelmi elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {//ha nem érkezik az adott eleme a postnak (checkbox!!!)
        $hiba['terms'] = '<span class="error">Kötelező mező!</span>';
    }

    //var_dump($hiba,'</pre>');
    if (empty($hiba)) {
        //var_dump('<pre>', $_POST, '</pre>');
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        echo '<pre>'.var_export($data,true).'</pre>';
        //Minden oké
        //Tároljuk el a kapott adatokat egy file-ban
        // ../4/adatok/user.txt
        //mappa ellenőrzése
        $dir = '../4/adatok/';
        if(!is_dir($dir)){//ha nincs mappa, elkészítjük
           // echo 'nincs mappa:'.$dir;
            mkdir($dir, 0755, true);//php.net: chmod()
        }
        $filename = 'users.json';
        //HF fopen, fread, fwrite, fclose
        //tárolás előtt tárolható (string) és visszafordítható módon alakítjuk a tömbünket
        //$dataSerial = serialize($data);//sorozat

        //több felh egy fileba.
        $users = [];
        if(is_file($dir.$filename)){//ha már van ilyen file, van már adat, ahhoz kell hozzátenni az ujat
            $fileContent = file_get_contents($dir.$filename);
            $users = json_decode($fileContent,true);
        }
        $users[] = $data;//bővítjük a users tömböt az aktuális adatokkal


        $dataJson = json_encode($users);
        file_put_contents($dir.$filename, $dataJson);
        //echo '<pre>';
        //var_dump($data, $dataSerial, $dataJson);

        //visszaalakítások
        //var_dump(unserialize($dataSerial));
        //var_dump(json_decode($dataJson, true));
        die('oké');
    }
}
//echo $_POST['name'];//EZT ÍGY SOHA ne vedd ki GETBŐL,POSTBÓL VAGY REQUESTBŐL
//helyes mód:
/*
$name = filter_input(INPUT_POST,'name');//php.net
var_dump($name);
*/
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap - regisztráció</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 500px;
            margin: auto;
            padding: 15px;
            display: flex;
            flex-flow: column;
        }

        label {
            width: 100%;
            margin: 15px auto 5px;
            display: flex;
            flex-flow: column;
        }

        .error {
            color: #f00;
            font-size: 0.7em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <h2>Regisztrációs adatlap</h2>
    <!--Név-->
    <label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="<?php echo getValue('name'); ?>">
        <?php
        //ha van hiba írjuk ki
        echo getError('name');
        ?>
    </label>

    <!--Email-->
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="<?php echo getValue('email'); ?>">
        <?php
        //ha van hiba írjuk ki
        echo getError('email');
        ?>
    </label>

    <!--Jelszó-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">
        <?php
        echo getError('password');//ha van hiba írjuk ki
        ?>
    </label>

    <!--Jelszó újra-->
    <label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">
        <?php
        echo getError('repassword');
        ?>
    </label>

    <!--adatvédelmi tájékoztató-->
    <label>
        <span>
            <?php
            //terms ki volt-e pipálva
            /*
                if(filter_input(INPUT_POST,'terms')){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            */
            $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

            ?>
            <input type="checkbox" name="terms" value="1" <?php echo $checked; ?>>
         Elolvastam és megértettem az <a href="#" target="_blank">adatvédelmi tájékoztató</a>ban leírtakat!</span>
        <?php
        echo getError('terms');
        ?>
    </label>
    <button>Regisztrálok</button>
</form>

</body>
</html>
<?php

/**
 * Input mezők value értékeit adja vissza ha ki volt töltve
 * @param $fieldName | mező name tulajdonsága ... name="fieldname"
 * @return mixed
 */
function getValue($fieldName)
{
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * Hibakiíró eljárás
 * $hiba['fieldname'] = 'Hibaüzenet' szerkezetből
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a $hiba tömb globális lesz
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}
