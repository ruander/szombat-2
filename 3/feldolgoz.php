<?php
echo '<pre>';//csak a dumpok formázása miatt
//űrlap adat feldolgozó file
//A GET tipusú adatot (url) $_GET szuperglobális tömb tartalmazza
var_dump($_GET);

//A POST tipusú adatot (a kérés fejlécében 'utazik') $_POST szuperglobális tömb tartalmazza
var_dump($_POST);

//A REQUEST a GET és a POST UNIÓJA adatnév egyezés esetén a POST az 'erősebb'
var_dump($_REQUEST);