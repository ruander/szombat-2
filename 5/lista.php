<?php
include 'settings.php';//mappa benne van éééé -ig

$huzasok_szama = filter_input(INPUT_GET, 'hsz', FILTER_VALIDATE_INT);

$dir .= $huzasok_szama . '/';
$week = date('W');
$fileName =$week . '.json';

//csak akkor csinálunk bármit, ha a file létezik, ha nincs-> vissza azindexre (választómenü)
if (!file_exists($dir . $fileName)) {
    header('location:index.php');
    exit();
}
$fileContent = file_get_contents($dir . $fileName);//filetartalom (json)
$szelvenyek = json_decode($fileContent, true);//tömb
//var_dump($szelvenyek);
//bejárjuk és táblázat

$table = '<a href="index.php">vissza a választó menühöz</a>
<h2>'.$week.'. heti szelvénylista</h2>
    <table class="list">';
$table .= '<tr>
            <th>id</th>            
            <th>név</th>            
            <th>email</th>            
            <th>tippek</th>            
           </tr>';
//szelvénysorok
foreach ($szelvenyek as $id => $szelveny) {
    $table .= '<tr>
                <td>' . $id . '</td>            
                <td>' . $szelveny['name'] . '</td>            
                <td>' . $szelveny['email'] . '</td>            
                <td>' . implode(',', $szelveny['tippek']) . '</td>            
               </tr>';
}

$table .= '</table>';

echo $table;

$style = '<style>
.list {
    border-collapse: collapse;
}

.list th,
.list td {
    padding:5px;
    border:1px solid #000;
}
</style>';

echo $style;