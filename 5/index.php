<?php
include 'settings.php';//beállítások
//@todo - HF 1. szelénylista opció csak akkor legyen ha létezik a file
//@todo extra HF: 2. lista nézeten ha még nincs sorsolás a listára akkor egy opcio hogy sorsolás-> 3. táblázat kiegészül találatok oszloppal ahol a 2 nél több találatot jelezzük és az egész sor zöld hátteret kap
//todo extra HF2: 4. régebbi szelvények (előző heti vagy még régebbi) elérésének lehetősége heti szelvénylisták: 01,02,03 ... mármint ami létezik
//az érvényes játéktipus tömb bejárásával elkészíthetünk egy 'választó menüt'
$menu = '<h1>Lottójáték  kiválasztása</h1>
<ul>';

foreach(VALID_GAME_TYPES as $k => $v){
    //van e már szelvénylista?
    //játékhoz tartozó file nevének összerakása
    $szelvenyekFile = $dir.$k. '/' . date('W'). '.json';

    $menu .= '<li><a href="lotto.php?hsz='.$k.'"> '.$k.'/'.$v.' Játék</a>';
    if(file_exists($szelvenyekFile)){//csak ha létezik a file, akkor tegyünk ki linket, ami a listára mutat
        $menu .= ' | <a href="lista.php?hsz='.$k.'">aktuális heti szelvénylista</a>';
    }

    $menu .='</li>';
}

$menu .= '</ul>';

echo $menu;