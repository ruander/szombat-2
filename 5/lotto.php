<?php
//erőforrás kezelése
include "settings.php";//beállítások
//lotto játék szelvény feladás
$huzasok_szama = filter_input(INPUT_GET,'hsz',FILTER_VALIDATE_INT);
//ha nem létező tipus, irány vissza a listára
if(!array_key_exists($huzasok_szama,VALID_GAME_TYPES)){
    header('location:index.php');//átirányítás a választó listára
    exit();
}
//határozzuk meg a limitet
$limit = VALID_GAME_TYPES[$huzasok_szama];

//ha kapunk űrlap elemeket, kezeljük őket
if (!empty($_POST)) {
    $hiba = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //NÉV legalább 3 karakter és trim
    $name = trim(filter_input(INPUT_POST, 'name'));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //tippek
    $tippek = filter_input(INPUT_POST, 'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    //echo '<pre>' . var_export($tippek, true) . '</pre>';
    //járjuk be a tippek tömböt
    //minden elemét teszteljük hogy 1-90 közötti egész szám-e
    // if(felétel1 [ || - or, && - és] feltétel2){...}
    // ha nem felel meg az érték a feltételnek a hiba tömb hasonló szerkezetére helyezzük el a
    //  $hiba['tippek'][n] = '<span class="error">Hibás formátum!</span>' szerkezetben;
    if(!is_array($tippek)){
        //valami nagy gond történt....
    }else{//megvan a tippek tömb
        $egyedi_tippek = array_unique($tippek);
        //echo '<pre>egyedi tippek:' . var_export($egyedi_tippek, true) . '</pre>';
        //szűrő opciók
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => $limit //erőforrás felhasználása ...
            ]
        ];

        foreach($tippek as $k => $tipp){
            $test = filter_var($tipp, FILTER_VALIDATE_INT,$options);
            if($test === false ){//
                $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
            }elseif( !array_key_exists($k,$egyedi_tippek) ){
                //ismétlődések kiszűrése: ha az egyedi_tippek tömbben az adott tipp kulcsa nem szerepel , az array_unique miatt került ki azaz ismétlődés
                $hiba['tippek'][$k] = '<span class="error">Már szepelt ez a szám!</span>';
            }
        }
    }

    if (empty($hiba)) {
        //adatok rendezése
        sort($tippek);//emelkedő sorrend

        $data = [
            'name' => $name,
            'email' => $email,
            'tippek' => $tippek,
        ];
        //echo '<pre>kész adatok:' . var_export($data, true) . '</pre>';
        //filenév és mappaműveletek

        $dir .= $huzasok_szama.'/';//alap dir a settingsből jön
        if(!is_dir($dir)){
            mkdir($dir, 0777, true);
        }
        $week = date('W');
        $fileName = $week.'.json';
        $szelvenyek = [];//itt lesznek a szelvények

        if(file_exists($dir.$fileName)){//ha léteznek már szelvények, betöltjük őket egy tömbként (decode)
            $szelvenyek = json_decode( file_get_contents($dir.$fileName),true);
        }
        //adjuk a szelvényekhez a mostanit
        $szelvenyek[] = $data;
        //jsonné alakítjuk és kiírjuk file-ba
        $szelvenyekJson = json_encode($szelvenyek);
        file_put_contents($dir.$fileName,$szelvenyekJson);

        //átirányítunk a formra
        header('location:'.$_SERVER['PHP_SELF']);
        exit();
    }
}

//űrlap összeállítása változóba
$form = '<form method="post">';//űrlap nyitás
$form .= "<h1>$huzasok_szama/$limit Játék</h1>";
$form .= '<a href="index.php">vissza</a>';
$form .= "<h2>Töltse ki a szelvényt:</h2>";
//név mező beillesztése
$form .= '<label style="display:block">
               Név <input type="text" value="' . getValue('name') . '" name="name" placeholder="Gipsz Jakab">';
//hiba befűzése a mezőbe
$form .= isset($hiba['name']) ? $hiba['name'] : '';
$form .= '</label>';
//email mező beillesztése
$form .= '<label style="display:block">
               Email <input type="text" value="' . getValue('email') . '" name="email" placeholder="you@example.com">';
//hiba befűzése a mezőbe
$form .= isset($hiba['email']) ? $hiba['email'] : '';
$form .= '</label>';
//tippek
for ($i = 1; $i <= $huzasok_szama; $i++) {
    //input elemek elkészítése a tippeknek
    $form .= '<label style="display:block">
               Tipp '.$i.'. <input maxlength="5" size="2" type="text" value="'.getValue('tippek', $i).'" name="tippek[' . $i . ']" placeholder="1-' . $limit . '">';
    //hiba befűzése a mezőbe
    $form .= isset( $hiba['tippek'][$i] ) ? $hiba['tippek'][$i] : '';
    $form .= '</label>';//mezőcimke lezárása
}


$form .= '<button>Tippek beküldése</button>
         </form>';//űrlap zárás
//kiírás egy lépésben
echo $form;

/**
 * Input mezők value értékeit adja vissza ha ki volt töltve
 * @param $fieldName | mező name tulajdonsága ... name="fieldname"
 * @param false|mixed $subKey| több dimenzió esetén a 2. dimenzó indexe
 * @return false|mixed - POSTBAN (vagy 2.dimenziójában, subKey esetén) található érték default szűrővel
 */
/**
 * @param $fieldName


 */
function getValue($fieldName, $subKey = false)
{
    if($subKey !== false){
        $elem = filter_input(INPUT_POST, $fieldName, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);//a komplett halmaz
        if(isset($elem[$subKey])){//keresett elem, ha létezik
            return $elem[$subKey];//térjünk vele vissza
        }
        return false;
    }
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * Hibakiíró eljárás
 * $hiba['fieldname'] = 'Hibaüzenet' szerkezetből
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a $hiba tömb globális lesz
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}
