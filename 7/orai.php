<pre><?php
//https://www.php.net/manual/en/book.mysqli.php
require "connect.php";//db csatlakozás 'betöltése'


$qry = "SELECT * FROM `employees`";//lekérés összeállítása

//lekérés vagy állj!
$results = mysqli_query($link,$qry) or die('DB hiba! '.mysqli_error($link));

//egy sor feldolgozása az eredményekből
$row = mysqli_fetch_row($results);
var_dump($row);

//egy sor feldolgozása az eredményekből
$row = mysqli_fetch_assoc($results);
var_dump($row);

//egy sor feldolgozása az eredményekből
$row = mysqli_fetch_array($results);
var_dump($row);

//egy sor feldolgozása az eredményekből
$row = mysqli_fetch_object($results);
var_dump($row);

/*//összes kibontása
$rows = mysqli_fetch_all($results, MYSQLI_ASSOC);
var_dump($rows);
////////////////////////////////////////
//egy sor feldolgozása az eredményekből
$row = mysqli_fetch_row($results);//már nincs eredmény ezért a returnje NULL lesz
var_dump($row);*/

//mind kibontása ciklusban

while( $row = mysqli_fetch_assoc($results) ){
    var_dump($row);
}
echo 'ok';