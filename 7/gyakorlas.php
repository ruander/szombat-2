<?php
require "connect.php";//db csatlakozás 'betöltése'

//19. Alkalmazott teljes neve, főnöke teljes nev , ha nincs neki akkor 'Boss'
//rossz megvalósítás
$qry = "SELECT CONCAT(firstname,' ',lastname) nev, reportsto FROM employees";
$results = mysqli_query($link,$qry) or die(mysqli_error($link));

$table = '<table border="1">';//table nyitás
$table .= '<tr>
             <th>név</th>
             <th>főnök</th>
           </tr>';//cimsor
while($row = mysqli_fetch_assoc($results)){//adatsorok
    //var_dump($row);
    //az alkalmazott reportsto eleme a lekérendő employeenumber-je (főnök)
    $qry = "SELECT CONCAT(firstname,' ',lastname) fonok FROM employees WHERE employeenumber = '{$row['reportsto']}' LIMIT 1";
    $resultFonok = mysqli_query($link,$qry) or die(mysqli_error($link));
    $rowFonok = mysqli_fetch_assoc($resultFonok);

    $table .= '<tr>
                 <td>'.$row['nev'].'</td>
                 <td>'.(isset($rowFonok['fonok'])?$rowFonok['fonok']:'BOSS').'</td>
               </tr>';//cimsor

}
$table .= '</table>';//table zárás

echo $table;

//jó megoldás
$qry = "SELECT 
	CONCAT(e.firstname,' ',e.lastname) nev,
    IF(
        e2.firstname IS NULL, 
        'BOSS', 
        CONCAT(e2.firstname,' ',e2.lastname)
    ) fonok  
FROM employees e
LEFT JOIN employees e2
ON 
	e2.employeenumber = e.reportsto";
$results = mysqli_query($link,$qry) or die(mysqli_error($link));

$table = '<table border="1">';//table nyitás
$table .= '<tr>
             <th>név</th>
             <th>főnök</th>
           </tr>';//cimsor
while($row = mysqli_fetch_assoc($results)){//adatsorok
    //var_dump($row);
    $table .= '<tr>
                 <td>'.$row['nev'].'</td>
                 <td>'.$row['fonok'].'</td>
               </tr>';//cimsor
}
$table .= '</table>';//table zárás