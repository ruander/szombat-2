<?php
/**
 * Regisztrációs űrlap minta
 */
if (!empty($_POST)) {
    //var_dump('<pre>',$_POST);
    //hibakezelés
    $hiba = [];//üres hibatömb, ide gyűjtjük a hibákat
    //...hibák kezelése, tárolása
    //Név min 3 karakter
    $name = trim(filter_input(INPUT_POST, 'name'));
    //$name = trim($name);//spacek eltávolítása mindkét oldalról
    $name = strip_tags($name);//html tagek eltávolítása a szövegből... (védelem)
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //echo $name;

    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //jelszó 1
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {//min 6 karakter
        $hiba['password'] = '<span class="error">Hibás formátum - minimum 6 karakter!</span>';
    } elseif ($password !== $repassword) {//jelszó2
        $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';

    } else {
        //jelszó elkódolása
        $password = password_hash($password, PASSWORD_BCRYPT);
    }


    //adatvédelmi elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {//ha nem érkezik az adott eleme a postnak (checkbox!!!)
        $hiba['terms'] = '<span class="error">Kötelező mező!</span>';
    }

    if (empty($hiba)) {
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        echo '<pre>'.var_export($data,true).'</pre>';

    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap - regisztráció</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 500px;
            margin: auto;
            padding: 15px;
            display: flex;
            flex-flow: column;
        }

        label {
            width: 100%;
            margin: 15px auto 5px;
            display: flex;
            flex-flow: column;
        }

        .error {
            color: #f00;
            font-size: 0.7em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <h2>Regisztrációs adatlap</h2>
    <!--Név-->
    <label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="<?php echo getValue('name'); ?>">
        <?php
        //ha van hiba írjuk ki
        echo getError('name');
        ?>
    </label>

    <!--Email-->
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="<?php echo getValue('email'); ?>">
        <?php
        //ha van hiba írjuk ki
        echo getError('email');
        ?>
    </label>

    <!--Jelszó-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">
        <?php
        echo getError('password');//ha van hiba írjuk ki
        ?>
    </label>

    <!--Jelszó újra-->
    <label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">
        <?php
        echo getError('repassword');
        ?>
    </label>

    <!--adatvédelmi tájékoztató-->
    <label>
        <span>
            <?php
            //terms ki volt-e pipálva
            /*
                if(filter_input(INPUT_POST,'terms')){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
            */
            $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

            ?>
            <input type="checkbox" name="terms" value="1" <?php echo $checked; ?>>
         Elolvastam és megértettem az <a href="#" target="_blank">adatvédelmi tájékoztató</a>ban leírtakat!</span>
        <?php
        echo getError('terms');
        ?>
    </label>
    <button>Regisztrálok</button>
</form>

</body>
</html>
<?php

/**
 * Input mezők value értékeit adja vissza ha ki volt töltve
 * @param $fieldName | mező name tulajdonsága ... name="fieldname"
 * @return mixed
 */
function getValue($fieldName)
{
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * Hibakiíró eljárás
 * $hiba['fieldname'] = 'Hibaüzenet' szerkezetből
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a $hiba tömb globális lesz
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}
