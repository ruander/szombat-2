<?php
$tomb = [];//tömb előkészítése
while(count($tomb)<10){
    $tomb[]=rand(1,100);
}
echo '<pre>'.var_export($tomb,true).'</pre>';
//rendezés érték szerint - emelkedő
sort($tomb);//referenciaátadás
echo '<pre>'.var_export($tomb,true).'</pre>';

//legnagyobb érték
$max = max($tomb);
echo "A tömb legnagyobb értéke: $max";
//legkisebb érték
$min = min($tomb);
echo "<br>A tömb legkisebb értéke: $min";
//a generált számok átlagértéke
//generáláskor is lehetne, de hamár megvannak akkor a array_sum megmondja az összeget, elosztjuk az elemszámmal és kész is
$sum = array_sum($tomb);
$average = $sum/count($tomb);
echo "<br>A generált számok átlagértéke: $average";