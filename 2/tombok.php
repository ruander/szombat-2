<?php

$tomb = [];//üres tömb
echo '<pre>';//a var_dump formázásához


$tomb[] = 'Új elem';//tömb bővítése automatikus indexen
$tomb[34] = '34es index';//tömb bővítése irányított indexen

var_dump($tomb);
$tomb[] = 'Ez is új elem';//a 35ös indexre kerül
$tomb[] = 234;
var_dump($tomb);
$tomb['email'] = 'hgy@iworkshop.hu';//tömb bővítése asszociatív indexre
var_dump($tomb);

//tömb újradefiniálása, variable override vagy redeclare
$tomb = [
    1 => 'valami',
    23 => 'más valami...',
    'email' => 'hgy@iworkshop.hu',
    'is_admin' => false,
];//tömb megadása értékekkel, irányított indexre
var_dump($tomb);

//tömb nem alakítható automatikus stringé (tömb->nem primitiv)
echo $tomb;//Array
//ha az érték az indexen primitív az kiírható
echo $tomb[23];
echo "<br>";
echo $tomb['email'];//hgy@iworkshop.hu

//műveletek
$tombHossz = count($tomb);//tömb elemszáma
var_dump($tombHossz);

//tömb feldolgozása
$tomb = [12, 12.5, 242, -42, 34];
var_dump($tomb);
$tombHossz = count($tomb);//tömb eleminek száma
//js tipusu minta
for ($i = 0; $i < $tombHossz; $i++) {
    echo '<br>' . $tomb[$i];
}
//php tömbbejárás
$tomb = [
    1 => 'valami',
    23 => 'más valami...',
    'email' => 'hgy@iworkshop.hu',
    'is_admin' => true,
];
var_dump($tomb);
/*
foreach(tomb AS index => ertek){
    //ciklusmag, ahol elérhető az aktuális értek és index
}
 */
foreach ($tomb as $key => $value) {
    //ciklusmag
    echo "<br>Az aktuális index: $key, az aktuális érték: $value";
}

foreach ($tomb as $v) {
    //ciklusmag
    echo "<br>Az aktuális érték: " . $v;
}

//több dimenziós tömb
$userData = [
    'id' => 15,
    'username' => 'HGy',
    'email' => 'hgy@iworkshop.hu',
    'verified' => true,
    'is_admin' => false,
    'favorite_colours' => ['deepink','lime','khaki'],
];
var_dump($userData);
//bejárás (több dimenziónál nem minden írható ki)
foreach($userData as $k => $v){
    //ha a kapott érték ($v) primitiv, akkor kiírjuk, különben kiírjuk hogy tömböt látunk az értéken
    if(  is_array($v)   ) {//tömb
        echo "<br>A kedvenc színek több elemet tartalmaz (tömb)";
    }else{//nem tömb
        echo "<br>k: $k - v:$v";
    }
}