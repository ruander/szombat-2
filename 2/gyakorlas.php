<?php
//írjuk ki a négyzetszámokat 1 és 10000 között , vel elválasztva
for ($i = 1; $i <= 100; $i++) {
    echo pow($i, 2) . ', ';
}
//írjuk ki a négyzetszámokat 1 és 1216 között , vel elválasztva
for ($i = 1; $i <= 100; $i++) {
    if (pow($i, 2) <= 1216) {
        echo pow($i, 2) . ', ';
    }
}
//optimálisabb: (de nem az igazi)
for ($i = 1; pow($i, 2) <= 1216; $i++) {
    $negyzet = pow($i, 2);
    if ( $negyzet <= 1216) {
        echo "$negyzet, '";
    }
}
//a while ciklus
/*
while(belépési feltétel vizsgálata){
    //ciklusmag
}
//hátultesztelő változata
do{
    //ciklusmag
    ...
}while(belépési feltétel vizsgálata)
 */
for($i=1;$i<=10;$i++){
    echo "<br>$i";
}//$i(11)
//ugyanez whileal
$i=1;//i reset
while($i<=10){
    echo "<br>".$i++;//léptetés művelet... után (++$i ...előtt)
}
$i = 1;
while(pow($i,2) <= 1216){
    echo "<br>".pow($i,2);
    $i++;
}

////////////////////////////////
/// további tömb műveletek/////
//////////////////////////////
//tömb értékekkel feltöltése ciklussal
//készítsünk egy 25 elemű tömböt 1-100 közötti véletlen egész értékekkel
$tomb = [];//tömb előkészítése
for($i=0;$i<100;$i++){
    //adjunk egy ujabb elemet a tömbhöz
    $tomb[]=rand(1,100);
}
var_dump($tomb);
$tomb2 = [];//tömb előkészítése
while(count($tomb2)<100){
    $tomb2[]=rand(1,100);
}
var_dump($tomb2);

